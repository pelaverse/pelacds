
<!-- README.md is generated from README.Rmd. Please edit that file -->
<!-- badges: start -->

[![pipeline
status](https://gitlab.univ-lr.fr/pelaverse/pelacds/badges/master/pipeline.svg)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![coverage
report](https://gitlab.univ-lr.fr/pelaverse/pelacds/badges/master/coverage.svg)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![version](https://img.shields.io/badge/version-0.2.0-blue)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8093319.svg)](https://doi.org/10.5281/zenodo.8093319)
<!-- badges: end -->

# pelaCDS

The package allows to perform routine Distance sampling analysis on
observation campaign sampled by plane. Data have to be sampled with
[SAMMOA](https://www.observatoire-pelagis.cnrs.fr/les-outils/sammoa/)
and linearised with
[pelaSIG](https://www.observatoire-pelagis.cnrs.fr/les-outils/pelagis/)
plug-in for QGIS.

The package offers functions to compute abundance estimation of interest
taxon by adjusting detection function. It is possible to integrate
covariates in the detection function (sea-state, meteo condition, …)
which can impact adjustment of detection function.

# Installation

To install pelaCDS package copy and paste the following code :

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr/",
  repo = "pelaverse/pelacds"
)
```

# Create a CDS analysis environment

pelaCDS include a function to create a R repository with all necessary
folders and sripts to transform raw data into estimation of abundance of
particular taxa.

To do so give the path where you want the analysis to be performed :

``` r
library(pelaCDS)
create_cds_proj("C:/user/pc_name/Documents/my_cds_analysis")
```

The repo : *“C:/user/pc_name/Documents/my_cds_analysis”* is organized as
follow :

    #>                                 levelName
    #> 1  my_cds_analysis                       
    #> 2   ¦--.Rhistory                         
    #> 3   ¦--data                              
    #> 4   ¦   ¦--effort                        
    #> 5   ¦   ¦--observation                   
    #> 6   ¦   °--strata                        
    #> 7   ¦--my_cds_analysis.Rproj             
    #> 8   °--R                                 
    #> 9       ¦--00_setup.R                    
    #> 10      ¦--01_change_colnames.R          
    #> 11      ¦--02_prepare_effort_and_obs.R   
    #> 12      ¦--CDS                           
    #> 13      ¦   ¦--01_adjust_plot_detection.R
    #> 14      ¦   ¦--02_make_table_summary.R   
    #> 15      ¦   °--03_detection_graph.R      
    #> 16      °--MCDS                          
    #> 17          °--01_mcds_adjusting.R

In order, you have to :

1.  Fill data repository with linearised effort and observation as
    shapefile. And a shapefile/data.frame containing area of each
    strata.
2.  Run script to clean and prepare data, and CDS and/or MCDS scripts
    depending on your analysis

# Functioning

Adjustment of detection function is based on the
[Distance](https://github.com/DistanceDevelopment/Distance) package.
