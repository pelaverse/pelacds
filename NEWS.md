# pelaCDS 0.2.0

<!-- NEWS.md is maintained by https://cynkra.github.io/fledge, do not edit -->

# pelaCDS 0.1.0.9002

* Add readme on the use of pelaCDS :package:


# pelaCDS 0.1.0.9001

* `test-prepare_effort_cds` adapt argument
* Update Documentation (argument accordance and examples) close #5, close #6, close #7
* Documentation now possible in Rmd
* Formatting `NEWS.md`


# pelaCDS 0.1.0.9000

* Fix Area issue R thinks that two equal numbers are different (#8)
* noms de colonnes en lower_case et avec underscore
* `strateSec` remplacé par `region_label` pour plus de lisibilité (nom plus explicite)
* In `prepare_effort_cds()` ajout de `unit_km = FALSE`
* enlève les segdata de add_obs
* ajout option multi_survey à prepare_effort/obs
* ajout de center_time
* remplace n_detected par n_detection (plus explicite) dans countdata_leg (il faudrait aussi remplacer n et y de countdata_seg dans pelaDSM par n_detected et n_ind)
* Correct data effort_sf and observation_sf
* Init test on clean_effort_name()
* Update function name
* homogenize colnames in clean_names functions


# pelaCDS 0.1.0

* Added a `NEWS.md` file to track changes to the package.
