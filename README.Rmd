---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

<!-- badges: start -->
[![pipeline status](https://gitlab.univ-lr.fr/pelaverse/pelacds/badges/master/pipeline.svg)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![coverage report](https://gitlab.univ-lr.fr/pelaverse/pelacds/badges/master/coverage.svg)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![version](https://img.shields.io/badge/version-0.2.0-blue)](https://gitlab.univ-lr.fr/pelaverse/pelacds/-/commits/master)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8093319.svg)](https://doi.org/10.5281/zenodo.8093319)
<!-- badges: end -->


```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
library(data.tree)
cds_project <-
  structure(
    list(
      V1 = c(
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis",
        "my_cds_analysis"
      ),
      V2 = c(
        ".Rhistory",
        "data",
        "data",
        "data",
        "data",
        "my_cds_analysis.Rproj",
        "R",
        "R",
        "R",
        "R",
        "R",
        "R",
        "R",
        "R",
        "R",
        "R"
      ),
      V3 = c(
        NA,
        NA,
        "effort",
        "observation",
        "strata",
        NA,
        NA,
        "00_setup.R",
        "01_change_colnames.R",
        "02_prepare_effort_and_obs.R",
        "CDS",
        "CDS",
        "CDS",
        "CDS",
        "MCDS",
        "MCDS"
      ),
      V4 = c(
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        NA,
        "01_adjust_plot_detection.R",
        "02_make_table_summary.R",
        "03_detection_graph.R",
        NA,
        "01_mcds_adjusting.R"
      ),
      V5 = c(
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_
      ),
      V6 = c(
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_,
        NA_character_
      ),
      pathString = c(
        "my_cds_analysis/.Rhistory",
        "my_cds_analysis/data",
        "my_cds_analysis/data/effort",
        "my_cds_analysis/data/observation",
        "my_cds_analysis/data/strata",
        "my_cds_analysis/my_cds_analysis.Rproj",
        "my_cds_analysis/R",
        "my_cds_analysis/R/00_setup.R",
        "my_cds_analysis/R/01_change_colnames.R",
        "my_cds_analysis/R/02_prepare_effort_and_obs.R",
        "my_cds_analysis/R/CDS",
        "my_cds_analysis/R/CDS/01_adjust_plot_detection.R",
        "my_cds_analysis/R/CDS/02_make_table_summary.R",
        "my_cds_analysis/R/CDS/03_detection_graph.R",
        "my_cds_analysis/R/MCDS",
        "my_cds_analysis/R/MCDS/01_mcds_adjusting.R"
      )
    ),
    row.names = c(NA,
                  -16L),
    class = "data.frame"
  )
```

# pelaCDS

The package allows to perform routine Distance sampling analysis on observation campaign sampled by plane. Data have to be sampled with [SAMMOA](https://www.observatoire-pelagis.cnrs.fr/les-outils/sammoa/) and linearised with [pelaSIG](https://www.observatoire-pelagis.cnrs.fr/les-outils/pelagis/) plug-in for QGIS.

The package offers functions to compute abundance estimation of interest taxon by adjusting detection function. It is possible to integrate covariates in the detection function (sea-state, meteo condition, ...) which can impact adjustment of detection function.

# Installation

To install pelaCDS package copy and paste the following code :

```{r eval = FALSE}
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr/",
  repo = "pelaverse/pelacds"
)
```

# Create a CDS analysis environment

pelaCDS include a function to create a R repository with all necessary folders and sripts to transform raw data into estimation of abundance of particular taxa.

To do so give the path where you want the analysis to be performed :

```{r eval=FALSE}
library(pelaCDS)
create_cds_proj("C:/user/pc_name/Documents/my_cds_analysis")
```

The repo : *"C:/user/pc_name/Documents/my_cds_analysis"* is organized as follow :

```{r echo = FALSE}
(mytree <- data.tree::as.Node(cds_project))
```

In order, you have to :

1.  Fill data repository with linearised effort and observation as shapefile. And a shapefile/data.frame containing area of each strata.
2.  Run script to clean and prepare data, and CDS and/or MCDS scripts depending on your analysis

# Functioning

Adjustment of detection function is based on the [Distance](https://github.com/DistanceDevelopment/Distance) package.
