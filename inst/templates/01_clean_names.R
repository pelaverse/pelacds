source("R/00_setup.R")
library(pelaCDS)

# Create mirror repository in "res/" --------------------------------------
path_save <- "res/01_clean_names"
if(!dir.exists(path_save)) {
  dir.create(path_save, recursive = T)
}


# Get effort and observation in "data/" -----------------------------------
effort <- st_read("data/effort/##########.shp")
observation <- st_read("data/observation/##########.shp")


# Clean effort and observation names --------------------------------------

# Change colnames of effort and observation with clean_names() ------------
standard_effort <- clean_effort_cds(effort, multi_survey = F, unit_km = TRUE) # unit_km = TRUE or FALSE
standard_obs <- clean_obs_cds(observation, multi_survey = F, unit_km = FALSE) # unit_km = TRUE or FALSE

# Navire should have distance of 500 instead of 200
standard_obs <- standard_obs %>%
  mutate(distance = ifelse(family == "Navire" & distance == set_units(200,m), set_units(500,m), distance)) %>%
  mutate(distance = set_units(distance, m))

# Save standard_effort and standard_obs in "res/" -------------------------
save(
  standard_effort,
  standard_obs,
  file = file.path(path_save, "standard_data.RData")
)

rm(effort, observation)
